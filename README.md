# Ad performance regression analysis

* Created a Python 3 notebook to analyze advertising sales performance and created a machine learning model to forecast highest impact of future investments.
* Authored C-level report interpreting model results and providing guidance on future advertising spend.

Quickstart:

* Download Python 3 notebook: LoganDowning-L06-AdRegression.ipynb
* Download datasource: Advertising.csv
* Open using Jupyter Notebook

Or view this [screen grab](images/LoganDowning-L06-AdRegression.png) of the entire notebook.

___

Used least-squares linear regression models to analyze the performance of investments in three channels: TV, newspaper and radio.

![Regression line drawn through best fit of scattered data](images/regression.png)

Explored impact of data outliers on the regressions by using bootstrap to evaluate their influence.

![Influence plot showing datapoints that might have outsized influence on regression](images/influencers.png)

Created C-level guidance to interpret model results and suggest investment additional ad dollars.

![Take-away guidance for C-level readers](images/takeaways.png)
